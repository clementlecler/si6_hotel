﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void fermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void annuler_Click(object sender, EventArgs e)
        {
            this.effacer();
        }

        private void effacer()
        {
            this.numericUpDown1.Text = "0";
            this.txtDescription.Text = "";
            this.cbHotel.Text = "";
            this.numericUpDown1.Focus();
        }

        private void cbHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            foreach (Hotel item in Persistance.getLesHotels())
            {
                cbHotel.Items.Add(item.Nom);
            }
        }

        private void enregistrer_Click(object sender, EventArgs e)
        {
            
            foreach (Hotel item in Persistance.getLesHotels())
            {
                if (item.Nom == cbHotel.Text)
                {
                    Persistance.ajouteChambre(item.Id, (int)numericUpDown1.Value, txtDescription.Text);
                }
            }

            MessageBox.Show("Ajout de la chambre dans l'Hotel" + cbHotel.Text + " à l'étage " + numericUpDown1.Value + " avec la description <" + txtDescription.Text + "> effectué", "Ajout Chambre", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
